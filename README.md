# BRCconsoleRemote

Dies ist eine minimale Kommandozeilenanwendung in QT für die Fernsteuerung des BRC.
Dafür gibt es eine zusätzliche Mission in der High-Level-Software. Die Fernsteuerung ist gut zum Testen der Hydraulik und sorgt für breites Grinsen der Teammitglieder.

Die Steuerung wurde unter Windows mit einem XBOX 360 Controller getestet. Sie sendet ein UDP Packet. Es kann vom Fahrzeug nur bei einer aktiven Testmission empfangen werden. Davor wird kein Server gestartet. Es kann nur gelenkt werden. Die Temposteuerung geschieht automatisch und hält die Mindestgeschwindigkeit.

Das Anhalten geschieht über das Emergency-Break-System, da es kein Produktiv-Modus ist und bei der Mindestgeschwindigkeit keine Schäden an den Reifen entstehen.
