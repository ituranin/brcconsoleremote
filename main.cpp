#include <QtGamepad>
#include <QWindow>
#include <QGuiApplication>
#include <QUdpSocket>
#include <iostream>

#define TARGET_MIN    25
#define TARGET_MAX   -25

#define TARGET_MIN_BRAKE     0
#define TARGET_MAX_BRAKE    60

using namespace std;

QUdpSocket* sender = NULL;

double normalize(double &value) {
    return ( ( (TARGET_MAX - (TARGET_MIN)) * (value - (-1)) ) / (1 - (-1)) ) + (TARGET_MIN);
}

double normalizeBrake(double &value) {
    return ( ( (TARGET_MAX_BRAKE - (TARGET_MIN_BRAKE)) * (value) ) / (1) ) + (TARGET_MIN_BRAKE);
}

void printLeftStickX(double value) {
    double normalized = normalize(value);
    int16_t factorized = (int16_t)(normalized * 10.0);

    QByteArray buffer;
    QDataStream message(&buffer, QIODevice::WriteOnly);
    message << (uint8_t)0;
    message << factorized;

    sender->writeDatagram(buffer, QHostAddress::Broadcast, 9000);

    cout << "leftStickX: " << value << " " << factorized << endl;
}

void printLeftTrigger(double value) {
    double normalized = normalizeBrake(value);
    int16_t factorized = (int16_t)(normalized * 10.0);

    QByteArray buffer;
    QDataStream message(&buffer, QIODevice::WriteOnly);
    message << (uint8_t)2;
    message << factorized;

    sender->writeDatagram(buffer, QHostAddress::Broadcast, 9000);
    cout << "leftTrigger: " << value << " " << factorized << endl;
}

void printRightTrigger(double value) {
    cout << "rightTrigger: " << value << endl;
}

void printXButton(bool value) {
    //clutch up
    QByteArray buffer;
    QDataStream message(&buffer, QIODevice::WriteOnly);
    message << (uint8_t)1;
    message << (uint16_t)200;

    sender->writeDatagram(buffer, QHostAddress::Broadcast, 9000);

    cout << "XButton: " << value << endl;
}

void printYButton(bool value) {
    //clutch down
    QByteArray buffer;
    QDataStream message(&buffer, QIODevice::WriteOnly);
    message << (uint8_t)1;
    message << (uint16_t)0;

    sender->writeDatagram(buffer, QHostAddress::Broadcast, 9000);

    cout << "YButton: " << value << endl;
}

int main(int argc, char *argv[])
{
    QGamepadManager *manager = QGamepadManager::instance();

    // workaround START https://bugreports.qt.io/browse/QTBUG-61553
    QGuiApplication app(argc, argv);
    QGuiApplication::processEvents();
    QWindow *window = new QWindow();
    window->show();
    delete window;
    // workaround END

    cout << "here0" << endl;

    auto gamepads = manager->connectedGamepads();
    if (gamepads.size() == 0) {
        return -1;
    }

    QGamepad gamepad;
    QObject::connect(&gamepad, &QGamepad::axisLeftXChanged, printLeftStickX);
    QObject::connect(&gamepad, &QGamepad::buttonL2Changed, printLeftTrigger);
    //QObject::connect(&gamepad, &QGamepad::buttonR2Changed, printRightTrigger);
    QObject::connect(&gamepad, &QGamepad::buttonXChanged, printXButton);
    QObject::connect(&gamepad, &QGamepad::buttonYChanged, printYButton);

    sender = new QUdpSocket();
    sender->bind(QHostAddress::AnyIPv4, 9000);

    return app.exec();
}
